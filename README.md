# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
A-Maze-ingly Retro Route Puzzle


* Quick summary

* comando per attraversare un dungeon e trovare gli oggetti

(Usando docker) docker-compose run maze-puzzle /code/manage.py traverse

(Usando una venv) python manage.py traverse


parametri:

-p=$PATH permette di specificare il path di un file, il path può essere assoluto o relativo ( in caso in cui non sia specificato viene utilizzato dungeon-json/example_1.json )

-s=$ROOM_ID permette di specificare la stanza di partenza, se non specificato la ricerca parte dalla stanza con ID=1


* comando per generare un dungeon casuale usabile per test

(Usando docker) docker-compose run maze-puzzle /code/manage.py dungeongenerator

(Usando una venv) python manage.py dungeongenerator

parametri:

-r=ROOMS_NUMBER permette di specificare il numero di stanze del dungeon, default=5 ( i file json vengono salvati in  dungeon-json/generated/ )


* comando per generare 10 dungeon casuali da 10 stanze

(Usando docker) docker-compose run maze-puzzle sh generate_dungeons.sh

(Usando una venv) sh generate_dungeons.sh



### How do I get set up? ###

* set up

è possibile usare docker oppure creare una virtualenv e installare i requirements con pip install -r requirements.txt

* Dependencies

Django<2.0

* Database configuration

il database è presente in .git ( db.sqlite3 ), si può cancellare il file e ricrearlo con docker-compose run maze-puzzle sh migrate.sh

* tests


docker-compose run maze-puzzle /code/manage.py test

(Usando una venv) python manage.py test

( sono nel file puzzle/tests.py )


### Who do I talk to? ###

* Repo owner or admin

__author__ = "Alain Maringoni"
__credits__ = ["Alain Maringoni"]
__version__ = "0.1"
__maintainer__ = "Alain Maringoni"
__email__ = "alainmaringoni@gmail.com"


* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)