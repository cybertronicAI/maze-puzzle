# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.core.management import call_command
from django.utils.six import StringIO

from .models import Dungeon, Room, ObjectInRoom, DEFAULT_FILE, DEFAULT_FILE_2

DEFAULT_ID = 1
DEFAULT_TEST_NAME = 'test'
DEFAULT_TEST_NAME_2 = 'Non esiste davvero'
DEFAULT_NORTH = 1
DEFAULT_EAST = 2
DEFAULT_SOUTH = 4
DEFAULT_WEST = 3
DEFAULT_ROOM_DICT = {"id": 3, "name": DEFAULT_TEST_NAME,
                     "north": DEFAULT_NORTH, "west": DEFAULT_WEST,
                     "south": DEFAULT_SOUTH, "east": DEFAULT_EAST,
                     "objects": [{"name": DEFAULT_TEST_NAME}, {"name": DEFAULT_TEST_NAME_2}]}
TEST_DEFAULT_FILE_MISSING = 'dungeon-json/missing_file.json'
TEST_DEFAULT_FILE_MISSING_ROOMS = 'dungeon-json/example_1_broken_no_rooms.json'


class QuestionModelTests(TestCase):
    def setUp(self):
        Dungeon.objects.create(id=DEFAULT_ID)
        Room.objects.create(id=DEFAULT_ID, position=1, name=DEFAULT_TEST_NAME,
                            north=DEFAULT_NORTH, dungeon=Dungeon.objects.get(id=DEFAULT_ID))
        ObjectInRoom.objects.create(id=DEFAULT_ID, name=DEFAULT_TEST_NAME)

    def test_has_object(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        object_in_room = ObjectInRoom.objects.get(id=DEFAULT_ID)
        room1.objects_in_room.add(object_in_room)

        self.assertIs(room1.has_objects(), True)

    def test_get_directions(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        self.assertIs(room1.get_directions()['north'], DEFAULT_NORTH)

    def test_get_neighbors(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        neighbors_list = room1.get_neighbors()
        self.assertIn(DEFAULT_NORTH, [room.position for room in room1.get_neighbors()])

    def test_open_door(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        room1.open_door(2, DEFAULT_SOUTH)
        self.assertIs(room1.get_directions()['north'], DEFAULT_NORTH)
        self.assertIs(room1.get_directions()['south'], DEFAULT_SOUTH)

    def test_has_closed_door(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        self.assertIs(room1.has_closed_door(), True)

    def test_has_open_door(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        self.assertIs(room1.has_open_door(DEFAULT_NORTH), False)

    def test_get_room_dict(self):
        room1 = Room.objects.get(id=DEFAULT_ID)
        room1_dict = room1.get_room_dict()
        self.assertIs(room1_dict['id'], 1)
        self.assertIs(room1_dict['north'], DEFAULT_NORTH)
        self.assertIn(room1_dict['name'], DEFAULT_TEST_NAME)

    def test_create_from_dict(self):
        room = Room.create_from_dict(DEFAULT_ROOM_DICT, dungeon=Dungeon.objects.get(id=DEFAULT_ID))
        self.assertIs(room.position, 3)
        self.assertIs(room.north, DEFAULT_NORTH)
        self.assertIs(room.east, DEFAULT_EAST)
        self.assertIs(room.south, DEFAULT_SOUTH)
        self.assertIs(room.west, DEFAULT_WEST)
        self.assertIn(room.name, DEFAULT_TEST_NAME)

        for object_in_room in room.objects_in_room.all():
            self.assertIn(object_in_room.name, [DEFAULT_TEST_NAME, DEFAULT_TEST_NAME_2])

    ##verifica alcuni errori possibili e poi il risultato esatto di example_1 ed example_2
    def test_command_output(self):
        out = StringIO()
        options = {'path': TEST_DEFAULT_FILE_MISSING}
        call_command('traverse', stdout=out, **options)
        self.assertIn('Json not valid', out.getvalue())

        options = {'path': TEST_DEFAULT_FILE_MISSING_ROOMS}
        call_command('traverse', stdout=out, **options)
        self.assertIn('Missing rooms list', out.getvalue())

        options = {'starting_room': 20}
        call_command('traverse', stdout=out, **options)
        self.assertIn('Starting room not valid', out.getvalue())

        options = {'starting_room': 2}
        call_command('traverse', stdout=out, **options)
        self.assertIn('Knife', out.getvalue())
        self.assertIn('Potted', out.getvalue())

        #options = {'path': TEST_DEFAULT_FILE_MISSING_ROOMS}
        #call_command('a_star', stdout=out, **options)
        #self.assertIn('Missing rooms list', out.getvalue())
