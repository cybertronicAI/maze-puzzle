# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import choice
import string
import json
# Create your models here.
from django.db import models
from django.core.files.base import ContentFile

DIRECTIONS = ["NORTH", "EAST", "SOUTH", "WEST"]
#north, east, south, west
DIRECTIONS_LIST = [0, 1, 2, 3]
STARTING_ROOM_NAME = "Hallway"
STARTING_ROOM_POSITION = 1
DEFAULT_ROOM_NAMES = ["Kitchen", "Garden", "Bedroom", "Restroom", "Empty room", "Useless room", "Heroes room"]
DEFAULT_OBJECTS_NAMES = ["Axe", "Knife", "Toy", "Pony", "Potted plant", "Monster", "Pillow", "Amazing object", "Batman"]
OBJECT_PROBABILITY = 0.20 #20% di possibilita che una stanza abbia un oggetto casuale
DEFAULT_FILE = 'dungeon-json/example_1.json'
DEFAULT_FILE_2 = 'dungeon-json/example_2.json'


class Room(models.Model):
    position = models.IntegerField(default=1, help_text="Chiamato ID nelle specifiche")
    name = models.CharField(max_length=200)
    north = models.IntegerField(null=True)
    east = models.IntegerField(null=True)
    south = models.IntegerField(null=True)
    west = models.IntegerField(null=True)
    objects_in_room = models.ManyToManyField('puzzle.ObjectInRoom', help_text="Chiamato objects nelle specifiche")
    dungeon = models.ForeignKey('puzzle.Dungeon')
    traverse_history = None

    def __unicode__(self):
        #return '%s %s %s %s %s %s' % (self.position, self.name, self.north, self.east, self.south, self.west)
        return '%s %s ' % (self.position, self.name)

    def add_object(self, object_name=None):
        ##crea un oggetto con il nome ricevuto, se il nome non e' fornito genera un oggetto casuale
        if object_name:
            object_in_room, created = ObjectInRoom.objects.get_or_create(name=object_name)
        else:
            object_in_room, created = ObjectInRoom.objects.get_or_create(name=ObjectInRoom.get_random_object())
        self.objects_in_room.add(object_in_room)

    def has_objects(self):
        if self.objects_in_room.all():
            return True
        else:
            return False

    def open_door(self, direction, position):
        if 0 == direction:
            self.north = position
        if 1 == direction:
            self.east = position
        if 2 == direction:
            self.south = position
        if 3 == direction:
            self.west = position
        self.save()

    def get_directions(self):
        directions = {}
        if self.north:
            directions['north'] = self.north
        if self.south:
            directions['south'] = self.south
        if self.east:
            directions['east'] = self.east
        if self.west:
            directions['west'] = self.west

        return directions

    def get_neighbors(self):
        neighbors = []
        if self.north:
            neighbors.append(Room.objects.get(position=self.north, dungeon=self.dungeon))
        if self.south:
            neighbors.append(Room.objects.get(position=self.south, dungeon=self.dungeon))
        if self.east:
            neighbors.append(Room.objects.get(position=self.east, dungeon=self.dungeon))
        if self.west:
            neighbors.append(Room.objects.get(position=self.west, dungeon=self.dungeon))

        return neighbors

    def get_room_dict(self):
        json_dict = {'id': self.position, "name": self.name,}
        if self.north:
            json_dict["north"] = self.north
        if self.south:
            json_dict["south"] = self.south
        if self.east:
            json_dict["east"] = self.east
        if self.west:
            json_dict["west"] = self.west

        if self.has_objects():
            object_in_room_list = []
            for object_in_room in self.objects_in_room.all():
                object_in_room_list.append({'name': object_in_room.name})
            json_dict["objects"] = object_in_room_list
        return json_dict

    def has_closed_door(self):
        if not self.north or not self.south or not self.east or not self.west:
            return True
        return False

    def has_open_door(self, direction):
        if 0 == direction and self.north:
            return True
        if 1 == direction and self.east:
            return True
        if 2 == direction and self.south:
            return True
        if 3 == direction and self.west:
            return True

        return False

    @staticmethod
    def get_random_name():
        return choice(DEFAULT_ROOM_NAMES)

    @staticmethod
    def create_from_dict(room_dict, dungeon):
        room = Room.objects.create(position=room_dict['id'], name=room_dict['name'], dungeon=dungeon)
        if "north" in room_dict:
            room.north = room_dict['north']
        if "south" in room_dict:
            room.south = room_dict['south']
        if "east" in room_dict:
            room.east = room_dict['east']
        if "west" in room_dict:
            room.west = room_dict['west']
        if "objects" in room_dict:
            for object_in_room in room_dict['objects']:
                room.add_object(object_in_room['name'])
        room.save()

        return room

    """
    def get_history(self, father_room=None):
        if self.traverse_history:
            return self.traverse_history
        else:
            return RoomTraverseHistory(self, father_room)
    """


class ObjectInRoom(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    @staticmethod
    def get_random_object():
        return choice(DEFAULT_OBJECTS_NAMES)

    @staticmethod
    def create_from_dict(object_dict):
        object_in_room, created = ObjectInRoom.objects.get_or_create(name=object_dict['name'])
        return object_in_room


class Dungeon(models.Model):
    name = models.CharField(max_length=10)
    json_file = models.FileField(upload_to='dungeon-json/generated/')

    def __unicode__(self):
        return self.name

    @staticmethod
    def name_generator(size=10, chars=string.ascii_uppercase + string.digits):
        return ''.join(choice(chars) for _ in range(size))

    def get_all_rooms(self):
        return Room.objects.filter(dungeon=self.id)

    def generate_json(self):
        dungeon_json = json.dumps({"rooms": [room.get_room_dict() for room in Room.objects.filter(dungeon=self.id)]})
        self.json_file.save(self.name + '.json', ContentFile(dungeon_json))
        return dungeon_json


class RoomTraverseHistory():
    def __init__(self, room, father_room=None):
        self.father_room = None
        if father_room:
            self.father_room = father_room

        if father_room:
            self.neighbors = room.get_neighbors().remove(father_room)
        else:
            self.neighbors = room.get_neighbors()
        self.visited_neighbors = []

    def visit_room(self, room):
        self.neighbors = self.neighbors.remove(room)
        self.visited_neighbors.append(room)

    def get_neighbors(self):
        if self.neighbors:
            return self.neighbors
        else:
            return self.father_room

    def has_neighbors(self):
        return self.neighbors