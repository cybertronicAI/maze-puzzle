import os
import json

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from puzzle.models import Dungeon, Room, ObjectInRoom, DEFAULT_FILE, DEFAULT_FILE_2, STARTING_ROOM_POSITION

FILE_PATH_ARGUMENT_NAME = 'file_path'


class Command(BaseCommand):
    help = 'walk through a dungeon and collect all objects'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.dungeon = None
        self.rooms = {}
        self.objects_in_rooms = {}
        self.starting_room = STARTING_ROOM_POSITION
        self.result_dict = {}
        self.total_objects_found = 0
        self.global_path = []
        self.position_objects_found = []

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        ##path del file
        parser.add_argument(
            '-p',
            '--path',
            action='store',
            dest=FILE_PATH_ARGUMENT_NAME,
            default=DEFAULT_FILE,
            help='Input file',
        )

        parser.add_argument(
            '-s',
            '--starting_room',
            action='store',
            dest='starting_room',
            default=STARTING_ROOM_POSITION,
            help='Starting room',
            )

    def handle(self, *args, **options):
        ##recupera il path del file
        if FILE_PATH_ARGUMENT_NAME in options:
            ##caso path assoluto
            if options[FILE_PATH_ARGUMENT_NAME].startswith('/'):
                file_path_complete = options[FILE_PATH_ARGUMENT_NAME]
            ##caso path relativo
            else:
                file_path_complete = os.path.join(settings.BASE_DIR, options[FILE_PATH_ARGUMENT_NAME])
        ##caso di default
        else:
            file_path_complete = os.path.join(settings.BASE_DIR, DEFAULT_FILE)

        self.stdout.write(self.style.SUCCESS('file_path_complete: %s ' % file_path_complete))
        starting_room = int(options['starting_room'])
        self.stdout.write(self.style.SUCCESS('starting_room: %s ' % starting_room))

        ##Apre il file JSON
        data = None
        try:
            with open(file_path_complete) as data_file:
                ##in caso di presenza di a capo nel file (\r oppure \n)
                ##vanno rimossi o correttamente inseriti (\\r \\n)
                ##.replace('\r\n', '') oppure .replace('\r\n', '\\r\\n')
                data = json.load(data_file)
        except Exception, e:
            self.stdout.write(self.style.ERROR('file json non valido: %s ' % e))
            return "Json not valid"

        ##esegue delle verifiche basilari sui dati in ingresso
        ##controlla che il json contenga un elenco di rooms
        if data and 'rooms' not in data:
            self.stdout.write(self.style.ERROR('file json non valido, manca la lista rooms'))
            return "Missing rooms list"

        ##legge e trasforma i dati
        dungeon = self.create_dungeon()
        for room_dict in data['rooms']:
            self.update_rooms(self.parse_room(room_dict, dungeon))

        ##verifica che la stanza di partenza (starting_room) sia presente nell'elenco delle stanze
        if starting_room not in self.rooms:
            self.stdout.write(self.style.ERROR('Stanza di partenza non valida'))
            return "Starting room not valid"

        ##calcola il percorso per raccogliere gli oggetti
        self.traverse(starting_room)

        return self.print_result()

    def create_dungeon(self):
        self.dungeon = Dungeon.objects.create(name=Dungeon.name_generator())
        return self.dungeon

    def parse_room(self, room_dict, dungeon):
        return Room.create_from_dict(room_dict, dungeon)

    def update_rooms(self, room):
        self.rooms[room.position] = room
        self.update_objects_in_rooms(room)
        return self.rooms

    def update_objects_in_rooms(self, room):
        if room.has_objects():
            self.objects_in_rooms[room.position] = room.objects_in_room.all()

    def update_global_path(self, path):
        if self.global_path:
            for node in path[1:]:
                self.global_path.append(node)
        else:
            for node in path:
                self.global_path.append(node)

    def has_found_all_objects(self, total_objects_found):
        if total_objects_found == len(self.objects_in_rooms):
            return True
        return False

    def find_path(self, starting_room, path=[]):
        cur_node = self.rooms[starting_room]
        path = path + [cur_node]

        ##trovato nuovo oggetto restituisce il path
        if cur_node.has_objects() and cur_node.position not in self.position_objects_found:
            self.position_objects_found.append(cur_node.position)
            self.total_objects_found += 1
            return path

        for node in cur_node.get_neighbors():
            if node not in path:
                temp_path = self.find_path(node.position, path)
                if temp_path:
                    return temp_path
        return None

    def traverse(self, starting_room):
        ##esegue il while fino a quando non trova tutti gli oggetti
        while self.total_objects_found < len(self.objects_in_rooms):
            path = self.find_path(starting_room)
            self.update_global_path(path)

            ##quando trova un oggetto esegue una nuova ricerca di path settando come stanza di partenza
            ##la stanza dell'oggetto appena trovato
            if path:
                starting_room = path[-1].position

    def print_result(self):
        ##elenco delle posizioni in cui sono gia' stati raccolti gli oggetti
        ## evita di stampare piu' volte la raccolta di una lista di oggetti
        printed_position_objects_found = []
        title = "{:<7}{:<30}{:<100}".format('ID', 'Room', 'Object Collected')
        line = ''
        for n in range(0, len(title)):
            line += '-'
        node_pretty_print = title + '\n'
        node_pretty_print += line + '\n'
        for node in self.global_path:
            if node.has_objects() and node.position not in printed_position_objects_found:
                printed_position_objects_found.append(node.position)
                pp = ''
                for object_in_room in node.objects_in_room.all():
                    pp += object_in_room.name + ' '
                node_pretty_print += "{:<7}{:<30}{:<100}".format(node.position, node.name, pp) + '\n'
            else:
                node_pretty_print += "{:<7}{:<30}".format(node.position, node.name) + '\n'

        return node_pretty_print
