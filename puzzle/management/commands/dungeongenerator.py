#!/usr/bin/env python

##Procedural Content Generation - Dungeon generation algorithm
##Crea un dungeon e il relativo file .json da usare per i test
##Come funziona: Genera la stanza di ingresso e poi delle stanze casuali partendo da questa
##e' una versione molto semplificata degli algoritmi Growing Tree per le generazione di dungeon casuali usati nei roguelike


from random import shuffle, random, randint, choice

from django.core.management.base import BaseCommand, CommandError

from puzzle.models import Room, Dungeon, DIRECTIONS_LIST, STARTING_ROOM_NAME, OBJECT_PROBABILITY, STARTING_ROOM_POSITION

DEFAULT_NUM_ROOMS = 5
MAX_PORT_NUMBER = 4
ROOMS_ARGUMENT_NAME = 'rooms'


class Command(BaseCommand):
    help = 'Create a new dungeon'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.dungeon = None
        self.rooms = []
        self.room_number = STARTING_ROOM_POSITION
        self.num_rooms = DEFAULT_NUM_ROOMS

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        #Rooms
        parser.add_argument(
            '-r',
            '--' + ROOMS_ARGUMENT_NAME,
            action='store',
            dest=ROOMS_ARGUMENT_NAME,
            default=DEFAULT_NUM_ROOMS,
            help='Maze rooms',
        )

    def handle(self, *args, **options):
        if ROOMS_ARGUMENT_NAME in options:
            self.num_rooms = int(options[ROOMS_ARGUMENT_NAME])

        ##crea un nuovo dungeon che contiene una lista di stanze pari a num_roooms
        ##parte dalla stanza in posizione 1 -> "Hallway"
        self.update_rooms(Room(position=self.room_number, name=STARTING_ROOM_NAME, dungeon=self.create_dungeon()))
        ##fino a quando non raggiunge il numero richiesto di stanze continua a generarle
        while self.room_number < self.num_rooms:
            ##seleziona una foglia del dungeon
            ##TO DO: Migliorare la selezione della foglia
            room = choice(self.rooms)
            if room.has_closed_door():
                self.walk(room)

        ##printa il dungeon
        for room in self.rooms:
            self.stdout.write(self.style.SUCCESS('room: %s -> %s ' % (room.position, room.get_directions())))
            if room.has_objects():
                self.stdout.write(self.style.SUCCESS('Oggetti: %s -> %s ' % (room.position, room.objects_in_room.all())))

        ##salva il dungeon in un file json
        self.stdout.write(self.style.SUCCESS('Dungeon Json: %s ' % (self.get_dungeon().generate_json())))
        self.stdout.write(self.style.SUCCESS('Maze successfully created "Rooms: %s"' % self.num_rooms))

    def walk(self, start_room):
        ##seleziona un numero casuale di porte da aprire (N, E, S, W)
        ports_to_open = randint(1, MAX_PORT_NUMBER)
        shuffle(DIRECTIONS_LIST)
        for direction in DIRECTIONS_LIST[:ports_to_open]:
            ##controlla che la stanza non abbia gia' una porta aperta in quella direzione
            ##e che non sia ancora stato raggiunto il numero massimo di stanze aperte
            if not start_room.has_open_door(direction) and self.room_number < self.num_rooms:
                new_room = self.create_room(start_room, direction)
                start_room.open_door(direction, new_room.position)
                self.update_rooms(new_room)

    def create_dungeon(self):
        self.dungeon = Dungeon.objects.create(name=Dungeon.name_generator())
        return self.dungeon

    def get_dungeon(self):
        return self.dungeon

    def create_room(self, father_room, father_direction):
        ##le porte sono in direzione opposta al padre
        room = Room(position=self.update_room_number(), name=Room.get_random_name(), dungeon=father_room.dungeon)
        if 0 == father_direction:
            room.south = father_room.position
        if 1 == father_direction:
            room.west = father_room.position
        if 2 == father_direction:
            room.north = father_room.position
        if 3 == father_direction:
            room.east = father_room.position
        ##aggiorna le porte aperte anche per la stanza di provenienza
        father_room.save()
        room.save()
        ##aggiunge in modo casuale uno o piu' oggetti alla stanza
        while random() < OBJECT_PROBABILITY:
            #self.stdout.write(self.style.SUCCESS('random: %s -> %s ' % (random(), OBJECT_PROBABILITY)))
            room.add_object()

        return room

    def update_rooms(self, room):
        self.rooms.append(room)
        return self.rooms

    def update_room_number(self):
        self.room_number += 1
        return self.room_number
